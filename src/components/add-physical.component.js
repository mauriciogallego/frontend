import React, { Component } from "react";
import TitularService from "../services/titular.service";
import revalidator from 'revalidator';

let schema = {
  properties: {
      rut: {
          type: 'integer',
          maxLength: 20,
          required: true,
          allowEmpty: false
      },
      name: {
          type: 'string',
          maxLength: 10,
          required: true,
          allowEmpty: false
      },
      lastname: {
        type: 'string',
        maxLength: 250,
        minLength: 1,
        required: true,
        allowEmpty: false
    },
    cc: {
      type: 'integer',
      maxLength: 20,
      required: true,
      allowEmpty: false
  },
  }
};

export default class AddPhysical extends Component {
  constructor(props) {
    super(props);
    this.onChangeRut = this.onChangeRut.bind(this);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeLastname = this.onChangeLastname.bind(this);
    this.onChangeCC = this.onChangeCC.bind(this);
    this.saveTitular = this.saveTitular.bind(this);
    this.newTitular = this.newTitular.bind(this);
    this.returnList = this.returnList.bind(this);
    this.state = {
      rut: null,
      name: "",
      lastname: "",
      cc: null,
      submitted: false,
      errors: {
        rut: '',
        name: '',
        lastname: '',
        cc: ''
      }
    };
  }

  onChangeRut(e) {
    let state = this.state;
    if(!isNaN(e.target.value)){
      let rut=null;
      if(e.target.value.length !== 0){
        rut= parseInt(e.target.value);
      }
      
      state.rut= rut;
      this.setState(state);
    }
    this.setErrors(state);
  }

  onChangeName(e) {
    let state = this.state;
    state.name= e.target.value;
    this.setState(state);
    this.setErrors(state);
  }

  onChangeLastname(e) {
    let state = this.state;
    state.lastname= e.target.value;
    this.setState(state);
    this.setErrors(state);
  }

  onChangeCC(e) {
    let state = this.state;
    if(!isNaN(e.target.value)){
      let cc=null;
      if(e.target.value.length !== 0){
        cc= parseInt(e.target.value);
      }
      state.cc= cc;
      this.setState(state);
    }
    this.setErrors(state);
  }

  saveTitular() {
    let errors = revalidator.validate(this.state, schema);
    
    if(errors.valid){
      var data = {
        rut: this.state.rut,
        name: this.state.name,
        lastname: this.state.lastname,
        cc: this.state.cc
      };
    TitularService.createPhysical(data)
      .then(response => {
        this.setState({
          submitted: true
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e.response);
        alert("No fue posible agregar a la persona. "+e.response.data.message);
      });
    }
    else{
      this.setErrors(this.state);
    }
  }

  setErrors(state){
    let errors = revalidator.validate(state, schema);
    let errorstates = {
      rut: '',
      name: '',
      lastname: '',
      cc: ''
    };
    errors.errors.forEach(error => {
      errorstates[error.property] = error.message; 
    });
    this.setState({
      errors: errorstates
    });
  }

  newTitular() {
    this.setState({
      rut: null,
      name: "",
      lastname: "",
      cc: null,
      submitted: false,
      errors: {
        rut: '',
        name: '',
        lastname: '',
        cc: ''
      }
    });
  }

  returnList() {    
    this.props.history.push('/titular/natural')
  }
  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>Se agrego correctamente!!</h4>
            <button className="btn btn-success mr-2" onClick={this.newTitular}>
              Agregar otro
            </button>
            <button onClick={this.returnList} className="btn btn-warning">
              ir a la lista
            </button>
          </div>
        ) : (
          <div>
            <div className="form-group">
              <label htmlFor="title">RUT</label>
              <input
                type="text"
                className="form-control"
                id="rut"
                required
                value={this.state.rut}
                onChange={this.onChangeRut}
                name="rut"
                required
              />
              {(this.state.errors.rut !== '')?<span className='text-danger'>{this.state.errors.rut}</span>:<div></div>}
            </div>
            <div className="form-group">
              <label htmlFor="title">Nombre</label>
              <input
                type="text"
                className="form-control"
                id="name"
                required
                value={this.state.name}
                onChange={this.onChangeName}
                name="name"
                required
              />
              {(this.state.errors.name !== '')?<span className='text-danger'>{this.state.errors.name}</span>:<div></div>}
            </div>
            <div className="form-group">
              <label htmlFor="title">Apellido</label>
              <input
                type="text"
                className="form-control"
                id="lastname"
                required
                value={this.state.lastname}
                onChange={this.onChangeLastname}
                name="lastname"
                required
              />
              {(this.state.errors.lastname !== '')?<span className='text-danger'>{this.state.errors.lastname}</span>:<div></div>}
            </div>
            <div className="form-group">
              <label htmlFor="title">Cedula</label>
              <input
                type="text"
                className="form-control"
                id="cc"
                required
                value={this.state.cc}
                onChange={this.onChangeCC}
                name="cc"
                required
              />
              {(this.state.errors.cc !== '')?<span className='text-danger'>{this.state.errors.cc}</span>:<div></div>}
            </div>
            <button onClick={this.saveTitular} className="btn btn-success mr-2">
              Agregar
            </button>
            <button onClick={this.returnList} className="btn btn-warning">
              Regresar
            </button>
          </div>
        )}
      </div>
    );
  }
}
