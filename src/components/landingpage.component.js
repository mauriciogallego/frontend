import React, { Component } from "react";


export default class LandingPage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <main role="main" class="inner cover">
          <h1 class="cover-heading">Sistema de visualizacion titulares de cuenta</h1>
          <p class="lead">Esta aplicación permite realizar la revisión del Ejercicio 1 - Titulares de cuenta corriente</p>
          <p class="lead">
            <a href="/titular/natural" class="btn btn-lg btn-secondary">Ver la información de las personas naturales</a>
          </p>
          <p class="lead">
            <a href="/titular/juridico" class="btn btn-lg btn-secondary">Ver la información de las personas juridicas</a>
          </p>
        </main>
      </div>
    );
  }
}
