import React, { Component } from "react";
import TitularService from "../services/titular.service";
import revalidator from 'revalidator';

let schema = {
  properties: {
      name: {
          type: 'string',
          maxLength: 10,
          required: true,
          allowEmpty: false
      },
      lastname: {
        type: 'string',
        maxLength: 250,
        minLength: 1,
        required: true,
        allowEmpty: false
    },
    cc: {
      type: 'integer',
      maxLength: 20,
      required: true,
      allowEmpty: false
  },
  }
};

export default class TitularPhyscal extends Component {
  constructor(props) {
    super(props);
    this.getTitular = this.getTitular.bind(this);
    this.updateTitular = this.updateTitular.bind(this);
    this.deleteTitular = this.deleteTitular.bind(this);
    this.returnList = this.returnList.bind(this);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeLastname = this.onChangeLastname.bind(this);
    this.onChangeCC = this.onChangeCC.bind(this);
    this.saveTitular = this.saveTitular.bind(this);
    this.newTitular = this.newTitular.bind(this);

    this.state = {
      currentTitular: {
        id: null,
        title: "",
        description: "",
        rut: null,
        name: "",
        lastname: "",
        cc: null
      },
      errors: {
        name: '',
        lastname: '',
        cc: ''
      },
      message: ""
    };
  }

  componentDidMount() {
    this.getTitular(this.props.match.params.id);
  }

  getTitular(id) {
    TitularService.getPhysicalById(id)
      .then(response => {
        this.setState({
          currentTitular: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updateTitular() {
    TitularService.updatePhysical(
      this.state.currentTitular.rut,
      this.state.currentTitular
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "El titular fue acutalizado correctamente!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deleteTitular() {    
    TitularService.deletePhysical(this.state.currentTitular.rut)
      .then(response => {
        console.log(response.data);
        this.props.history.push('/titular/natural')
      })
      .catch(e => {
        console.log(e);
      });
  }

  returnList() {    
    this.props.history.push('/titular/natural')
  }

  onChangeName(e) {
    let state = this.state;
    state.currentTitular.name= e.target.value;
    this.setState(state);
    this.setErrors(state);
  }

  onChangeLastname(e) {
    let state = this.state;
    state.currentTitular.lastname= e.target.value;
    this.setState(state);
    this.setErrors(state);
  }

  onChangeCC(e) {
    let state = this.state;
    if(!isNaN(e.target.value)){
      let cc=null;
      if(e.target.value.length !== 0){
        cc= parseInt(e.target.value);
      }
      state.currentTitular.cc= cc;
      this.setState(state);
    }
    this.setErrors(state);
  }

  saveTitular() {
    let errors = revalidator.validate(this.state, schema);
    
    if(errors.valid){
      var data = {
        rut: this.state.rut,
        name: this.state.name,
        lastname: this.state.lastname,
        cc: this.state.cc
      };
    TitularService.createPhysical(data)
      .then(response => {
        this.setState({
          submitted: true
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e.response);
        alert("No fue posible agregar a la persona. "+e.response.data.message);
      });
    }
    else{
      this.setErrors(this.state);
    }
  }

  setErrors(state){
    let errors = revalidator.validate(state.currentTitular, schema);
    let errorstates = {
      name: '',
      lastname: '',
      cc: ''
    };
    errors.errors.forEach(error => {
      errorstates[error.property] = error.message; 
    });
    this.setState({
      errors: errorstates
    });
  }

  newTitular() {
    this.setState({
      currentTitular: {
        rut: null,
        name: "",
        lastname: "",
        cc: null
      },
      submitted: false,
      errors: {
        name: '',
        lastname: '',
        cc: ''
      }
    });
  }

  render() {
    const { currentTitular } = this.state;

    return (
      <div>
        {currentTitular ? (
          <div className="edit-form">
            <h4>Editar Persona Natural</h4>
            <form>
              <div className="form-group">
                <label htmlFor="rut">RUT</label>
                <input
                  type="text"
                  className="form-control"
                  id="rut"
                  value={currentTitular.rut}
                  name="rut"
                  disabled
                />
                {(this.state.errors.rut !== '')?<span className='text-danger'>{this.state.errors.rut}</span>:<div></div>}
              </div>
            <div className="form-group">
              <label htmlFor="name">Nombre</label>
              <input
                type="text"
                className="form-control"
                id="name"
                value={currentTitular.name}
                onChange={this.onChangeName}
                name="name"
                required
              />
              {(this.state.errors.name !== '')?<span className='text-danger'>{this.state.errors.name}</span>:<div></div>}
            </div>
            <div className="form-group">
              <label htmlFor="lastname">Apellido</label>
              <input
                type="text"
                className="form-control"
                id="lastname"
                value={currentTitular.lastname}
                onChange={this.onChangeLastname}
                name="lastname"
                required
              />
              {(this.state.errors.lastname !== '')?<span className='text-danger'>{this.state.errors.lastname}</span>:<div></div>}
            </div>
            <div className="form-group">
              <label htmlFor="cc">Cedula</label>
              <input
                type="text"
                className="form-control"
                id="cc"
                value={currentTitular.cc}
                onChange={this.onChangeCC}
                name="cc"
                required
              />
              {(this.state.errors.cc !== '')?<span className='text-danger'>{this.state.errors.cc}</span>:<div></div>}
            </div>
            </form>

            <button
              className="badge badge-danger mr-2"
              onClick={this.deleteTitular}
            >
              Eliminar
            </button>

            <button
              type="submit"
              className="badge badge-success mr-2"
              onClick={this.updateTitular}
            >
              Actualizar
            </button>
            
            <button
              className="badge badge-warning mr-2"
              onClick={this.returnList}
            >
              Regresar
            </button>
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p>Por favor debe elegir un titular...</p>
          </div>
        )}
      </div>
    );
  }
}
