import React, { Component } from "react";
import TitularService from "../services/titular.service";
import revalidator from 'revalidator';

let schema = {
  properties: {
      businessname: {
          type: 'string',
          maxLength: 100,
          required: true,
          allowEmpty: false
      },
      foundationyear: {
        required: true,
        allowEmpty: false
    }
  }
};

export default class TitularLegal extends Component {
  constructor(props) {
    super(props);
    this.getTitular = this.getTitular.bind(this);
    this.updateTitular = this.updateTitular.bind(this);
    this.deleteTitular = this.deleteTitular.bind(this);
    this.returnList = this.returnList.bind(this);
    this.onChangeBusinessname = this.onChangeBusinessname.bind(this);
    this.onChangeFoundationyear = this.onChangeFoundationyear.bind(this);
    
    this.state = {
      currentTitular: {
        rut: null,
        businessname: "",
        foundationyear: "",
      },
      errors: {
        rut: '',
        businessname: '',
        foundationyear: '',
      },
      message: ""
    };
  }

  componentDidMount() {
    this.getTitular(this.props.match.params.id);
  }

  getTitular(id) {
    TitularService.getLegalById(id)
      .then(response => {
        response.data.foundationyear = new Date(response.data.foundationyear).toISOString().split('T')[0];
        this.setState({
          currentTitular: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updateTitular() {
    TitularService.updateLegal(
      this.state.currentTitular.rut,
      this.state.currentTitular
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "El titular fue acutalizado correctamente!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deleteTitular() {    
    TitularService.deleteLegal(this.state.currentTitular.rut)
      .then(response => {
        console.log(response.data);
        this.props.history.push('/titular/juridico')
      })
      .catch(e => {
        console.log(e);
      });
  }

  returnList() {    
    this.props.history.push('/titular/juridico')
  }

  onChangeBusinessname(e) {
    let state = this.state;
    state.currentTitular.businessname= e.target.value;
    this.setState(state);
    this.setErrors(state);
  }

  onChangeFoundationyear(e) {
    console.log('change Foundation')

    let state = this.state;
    console.log(state)
    state.currentTitular.foundationyear= e.target.value;
    this.setState(state);
    this.setErrors(state);
  }

  setErrors(state){
    let errors = revalidator.validate(state.currentTitular, schema);
    let errorstates = {
      name: '',
      lastname: '',
      cc: ''
    };
    errors.errors.forEach(error => {
      errorstates[error.property] = error.message; 
    });
    this.setState({
      errors: errorstates
    });
  }

  render() {
    const { currentTitular } = this.state;
    console.log(currentTitular.foundationyear)

    return (
      <div>
        {currentTitular ? (
          <div className="edit-form">
            <h4>Editar Persona Juridica</h4>
            <form>
              <div className="form-group">
                <label htmlFor="rut">RUT</label>
                <input
                  type="text"
                  className="form-control"
                  id="rut"
                  value={currentTitular.rut}
                  name="rut"
                  disabled
                />
                {(this.state.errors.rut !== '')?<span className='text-danger'>{this.state.errors.rut}</span>:<div></div>}
              </div>
            <div className="form-group">
              <label htmlFor="businessname">Razón Social</label>
              <input
                type="text"
                className="form-control"
                id="businessname"
                value={currentTitular.businessname}
                onChange={this.onChangeBusinessname}
                name="businessname"
                required
              />
              {(this.state.errors.businessname !== '')?<span className='text-danger'>{this.state.errors.businessname}</span>:<div></div>}
            </div>
            <div className="form-group">
              <label htmlFor="foundationyear">Fecha de fundación</label>
              <input
                type="date"
                className="form-control"
                id="foundationyear"
                value={currentTitular.foundationyear}
                onChange={this.onChangeFoundationyear}
                name="foundationyear"
                required
              />
              {(this.state.errors.foundationyear !== '')?<span className='text-danger'>{this.state.errors.foundationyear}</span>:<div></div>}
            </div>
            </form>

            <button
              className="badge badge-danger mr-2"
              onClick={this.deleteTitular}
            >
              Eliminar
            </button>

            <button
              type="submit"
              className="badge badge-success mr-2"
              onClick={this.updateTitular}
            >
              Actualizar
            </button>
            
            <button
              className="badge badge-warning mr-2"
              onClick={this.returnList}
            >
              Regresar
            </button>
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p>Por favor debe elegir un titular...</p>
          </div>
        )}
      </div>
    );
  }
}
