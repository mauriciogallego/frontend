import React, { Component } from "react";
import TitularService from "../services/titular.service";
import { Link } from "react-router-dom";

export default class TitularLegalList extends Component {
  constructor(props) {
    super(props); 
    this.retriveTitulares = this.retriveTitulares.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveTitular = this.setActiveTitular.bind(this);

    this.state = {
      titulares: [],
      currentTitular: null,
      currentIndex: -1
    };
  }

  componentDidMount() {
    this.retriveTitulares();
  }

  retriveTitulares() {
    TitularService.getAllLegal()
      .then(response => {
        this.setState({
          titulares: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retriveTitulares();
    this.setState({
      currentTitular: null,
      currentIndex: -1
    });
  }

  setActiveTitular(titular, index) {
    titular.foundationyear = new Date(titular.foundationyear).toISOString().split('T')[0];
    this.setState({
      currentTitular: titular,
      currentIndex: index
    });
  }

  render() {
    const { titulares, currentTitular, currentIndex } = this.state;

    return (
      <div className="list row">
        
        <div className="col-md-8">
          <h4>Lista de los Titulares persona juridica</h4>

          <ul className="list-group">
            {titulares &&
              titulares.map((titular, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveTitular(titular, index)}
                  key={index}
                >
                  {titular.rut} {titular.businessname}
                </li>
              ))}
          </ul>
          <Link
              to="/titular/juridico/add"
              className="m-3 btn btn-sm btn-success"
            >
              Agregar nueva empresa
            </Link>
        </div>
        <div className="col-md-4">
          {currentTitular ? (
            <div>
              <h4>Titular</h4>
              <div>
                <label>
                  <strong>RUT:</strong>
                </label>{" "}
                {currentTitular.rut}
              </div>
              <div>
                <label>
                  <strong>Razon social:</strong>
                </label>{" "}
                {currentTitular.businessname}
              </div>
              <div>
                <label>
                  <strong>Año fundacion:</strong>
                </label>{" "}
                {currentTitular.foundationyear}
              </div>
              <Link
                to={"/titular/juridico/" + currentTitular.rut}
                className="badge badge-warning"
              >
                Editar
              </Link>
            </div>
          ) : (
            <div>
              <br />
              <p>Por favor clic en un titular persona juridica...</p>
            </div>
          )}
        </div>
      </div>
      
    );
  }
}
