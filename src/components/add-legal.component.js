import React, { Component } from "react";
import TitularService from "../services/titular.service";
import revalidator from 'revalidator';

let schema = {
  properties: {
      rut: {
          type: 'integer',
          maxLength: 20,
          required: true,
          allowEmpty: false
      },
      businessname: {
          type: 'string',
          maxLength: 100,
          required: true,
          allowEmpty: false
      },
      foundationyear: {
        required: true,
        allowEmpty: false
    }
  }
};

export default class AddPhysical extends Component {
  constructor(props) {
    super(props);
    this.onChangeRut = this.onChangeRut.bind(this);
    this.onChangeBusinessname = this.onChangeBusinessname.bind(this);
    this.onChangeFoundationyear = this.onChangeFoundationyear.bind(this);
    
    this.saveTitular = this.saveTitular.bind(this);
    this.newTitular = this.newTitular.bind(this);
    this.returnList = this.returnList.bind(this);
    this.state = {
      rut: null,
      businessname: "",
      foundationyear: "",
      submitted: false,
      errors: {
        rut: '',
        businessname: '',
        foundationyear: '',
      }
    };
  }

  onChangeRut(e) {
    let state = this.state;
    if(!isNaN(e.target.value)){
      let rut=null;
      if(e.target.value.length !== 0){
        rut= parseInt(e.target.value);
      }
      
      state.rut= rut;
      this.setState(state);
    }
    this.setErrors(state);
  }

  onChangeBusinessname(e) {
    let state = this.state;
    state.businessname= e.target.value;
    this.setState(state);
    this.setErrors(state);
  }

  onChangeFoundationyear(e) {
    let state = this.state;
    state.foundationyear= e.target.value;
    this.setState(state);
    this.setErrors(state);
  }

  saveTitular() {
    let errors = revalidator.validate(this.state, schema);
    console.log(errors)
    if(errors.valid){
      var data = {
        rut: this.state.rut,
        businessname: this.state.businessname,
        foundationyear: this.state.foundationyear
      };
    TitularService.createLegal(data)
      .then(response => {
        this.setState({
          submitted: true
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e.response);
        alert("No fue posible agregar a la persona. "+e.response.data.message);
      });
    }
    else{
      this.setErrors(this.state);
    }
  }

  setErrors(state){
    let errors = revalidator.validate(state, schema);
    let errorstates = {
      rut: '',
      businessname: '',
      foundationyear: ''
    };
    errors.errors.forEach(error => {
      errorstates[error.property] = error.message; 
    });
    this.setState({
      errors: errorstates
    });
  }

  newTitular() {
    this.setState({
      rut: null,
      businessname: "",
      foundationyear: "",
      submitted: false,
      errors: {
        rut: '',
        businessname: '',
        foundationyear: '',
      }
    });
  }

  returnList() {    
    this.props.history.push('/titular/juridico')
  }
  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>Se agrego correctamente!!</h4>
            <button className="btn btn-success mr-2" onClick={this.newTitular}>
              Agregar otro
            </button>
            <button onClick={this.returnList} className="btn btn-warning">
              ir a la lista
            </button>
          </div>
        ) : (
          <div>
            <div className="form-group">
              <label htmlFor="title">RUT</label>
              <input
                type="text"
                className="form-control"
                id="rut"
                required
                value={this.state.rut}
                onChange={this.onChangeRut}
                name="rut"
                required
              />
              {(this.state.errors.rut !== '')?<span className='text-danger'>{this.state.errors.rut}</span>:<div></div>}
            </div>
            <div className="form-group">
              <label htmlFor="title">Razón Social</label>
              <input
                type="text"
                className="form-control"
                id="businessname"
                required
                value={this.state.businessname}
                onChange={this.onChangeBusinessname}
                name="businessname"
                required
              />
              {(this.state.errors.businessname !== '')?<span className='text-danger'>{this.state.errors.businessname}</span>:<div></div>}
            </div>
            <div className="form-group">
              <label htmlFor="title">Fecha de fundación</label>
              <input
                type="date"
                className="form-control"
                id="foundationyear"
                required
                value={this.state.foundationyear}
                onChange={this.onChangeFoundationyear}
                name="foundationyear"
                required
              />
              {(this.state.errors.foundationyear !== '')?<span className='text-danger'>{this.state.errors.foundationyear}</span>:<div></div>}
            </div>
            <button onClick={this.saveTitular} className="btn btn-success mr-2">
              Agregar
            </button>
            <button onClick={this.returnList} className="btn btn-warning">
              Regresar
            </button>
          </div>
        )}
      </div>
    );
  }
}
