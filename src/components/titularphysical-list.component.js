import React, { Component } from "react";
import TitularService from "../services/titular.service";
import { Link } from "react-router-dom";

export default class TitularPhysicalList extends Component {
  constructor(props) {
    super(props);
    this.retriveTitulares = this.retriveTitulares.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveTitular = this.setActiveTitular.bind(this);

    this.state = {
      titulares: [],
      currentTitular: null,
      currentIndex: -1
    };
  }

  componentDidMount() {
    this.retriveTitulares();
  }

  retriveTitulares() {
    TitularService.getAllPhysical()
      .then(response => {
        this.setState({
          titulares: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retriveTitulares();
    this.setState({
      currentTitular: null,
      currentIndex: -1
    });
  }

  setActiveTitular(tutorial, index) {
    this.setState({
      currentTitular: tutorial,
      currentIndex: index
    });
  }


  render() {
    const { titulares, currentTitular, currentIndex } = this.state;

    return (
      <div className="list row">
        
        <div className="col-md-8">
          <h4>Lista de los Titulares persona natural</h4>

          <ul className="list-group">
            {titulares &&
              titulares.map((titular, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveTitular(titular, index)}
                  key={index}
                >
                  {titular.name} {titular.lastname}
                </li>
              ))}
          </ul>
            <Link
              to="/titular/natural/add"
              className="m-3 btn btn-sm btn-success"
            >
              Agregar Nueva persona
            </Link>
        </div>

        <div className="col-md-4">
          {currentTitular ? (
            <div>
              <h4>Titular</h4>
              <div>
                <label>
                  <strong>RUT:</strong>
                </label>{" "}
                {currentTitular.rut}
              </div>
              <div>
                <label>
                  <strong>Name:</strong>
                </label>{" "}
                {currentTitular.name}
              </div>
              <div>
                <label>
                  <strong>Apellido:</strong>
                </label>{" "}
                {currentTitular.lastname}
              </div>
              <div>
                <label>
                  <strong>CC:</strong>
                </label>{" "}
                {currentTitular.cc}
              </div>

              <Link
                to={"/titular/natural/" + currentTitular.rut}
                className="badge badge-warning"
              >
                Editar
              </Link>

            </div>
          ) : (
            <div>
              <br />
              <p>Por favor clic en un titular persona natural...</p>
            </div>
          )}
        </div>
      </div>
      
    );
  }
}
