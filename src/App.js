import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AddPhysical from "./components/add-physical.component";
import AddLegal from "./components/add-legal.component";
import TitularPhysical from "./components/titularphysical.component";
import TitularPhysicalList from "./components/titularphysical-list.component";
import TitularLegalList from "./components/titularlegal-list.component";
import TitularLegal from "./components/titularlegal.component";
import LandingPage from "./components/landingpage.component";

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <nav className="navbar navbar-expand navbar-dark bg-dark">
            <a href="/" className="navbar-brand">
              Sistema
            </a>
            <div className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={"/titular/natural"} className="nav-link">
                  Personas naturales
                </Link>
              </li>
              <li className="nav-item">
                <Link to={"/titular/juridico"} className="nav-link">
                  Personas juridicas
                </Link>
              </li>
            </div>
          </nav>

          <div className="container mt-3">
            <Switch>
              <Route exact path="/" component={LandingPage} />
              <Route exact path="/titular/natural" component={TitularPhysicalList} />
              <Route exact path="/titular/natural/add" component={AddPhysical} />
              <Route path="/titular/natural/:id" component={TitularPhysical} />
              <Route exact path="/titular/juridico" component={TitularLegalList} />
              <Route exact path="/titular/juridico/add" component={AddLegal} />
              <Route path="/titular/juridico/:id" component={TitularLegal} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
