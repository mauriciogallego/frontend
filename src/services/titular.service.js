import http from "../http-common";

class TitularService {
  getAllPhysical() {
    return http.get("/accountholder/physical/findAll");
  }

  getPhysicalById(id) {
    return http.get(`/accountholder/physical/${id}`);
  }

  createPhysical(data) {
    return http.post("/accountholder/physical/", data);
  }

  updatePhysical(rut, data) {
    return http.put(`/accountholder/physical/${rut}`, data);
  }

  deletePhysical(rut) {
    return http.delete(`/accountholder/physical/${rut}`);
  }

  getAllLegal() {
    return http.get("/accountholder/legal/findAll");
  }

  getLegalById(id) {
    return http.get(`/accountholder/legal/${id}`);
  }

  createLegal(data) {
    return http.post("/accountholder/legal/", data);
  }

  updateLegal(rut, data) {
    return http.put(`/accountholder/legal/${rut}`, data);
  }

  deleteLegal(rut) {
    return http.delete(`/accountholder/legal/${rut}`);
  }
 
}

export default new TitularService();